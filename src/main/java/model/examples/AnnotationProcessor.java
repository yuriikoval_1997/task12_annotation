package model.examples;

import custom_annotations.examples_with_explanation.Service;

import java.lang.annotation.Annotation;

public class AnnotationProcessor {
    //Also possible to use instead of wildcard
//    public static <T> void inspectServiceGen(Class<T> serviceClass){
//    }

    public static void inspectService(Class<?> serviceClass){
        Class<? extends Annotation> annClass = Service.class;
        if (serviceClass.isAnnotationPresent(Service.class)){
//            Service serviceAnn = serviceClass.getAnnotation(annClass); // incomparable types,
// because annClass can be Annotation, or Service,  or a child of Service. But it's only suitable to use Service or its child!!!
            Service serviceAnn = serviceClass.getAnnotation(Service.class);
        }
    }

    public static void inspectAnotherService(Class<?> serviceClass){
        Class<? extends Service> ann = Service.class;
        if (serviceClass.isAnnotationPresent(ann)){
            Service serviceAnn = serviceClass.getAnnotation(ann); // Here everything works correctly)))
        }
    }
}
