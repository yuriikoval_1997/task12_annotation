package model.examples;

import custom_annotations.examples_with_explanation.Init;
import custom_annotations.examples_with_explanation.Service;

@Service(name = "The most laziest service")
public class LazyService {
    private int serviceId;

    @Init
    public void lazyInit() throws Exception{
    }
}
