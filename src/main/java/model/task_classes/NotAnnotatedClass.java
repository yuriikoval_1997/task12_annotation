package model.task_classes;

public class NotAnnotatedClass {
    private String field_1 = "the newValue of the first field";
    private String field_2 = "the newValue of second field";
    private String field_3 = "the newValue of third field";

    public void method_1(){
        System.out.println("printed from method_1");
    }

    public void method_2(){
        System.out.println("printed from method_2");
    }

    public void method_3(){
        System.out.println("printed from method_3");
    }
}
