package model.task_classes;

import custom_annotations.annotaion_for_task.*;

@ClassAnnotation(idValue = 1, stringValue = "example of a class", state = State.FIRST_OPTION)
public class AnnotatedClassSample {
    private String field_1 = "the newValue of the first field";
    private String field_2 = "the newValue of second field";
    private String field_3 = "the newValue of third field";
    @FieldAnnotation
    private String annotatedField = "first annotated field";
    @FieldAnnotation(changeValue = true)
    private String anotherAnnotatedField = "second annotated field";


    @MethodAnnotation(invokeMethod = true)
    public void printHello(){
        System.out.println("Hello world!");
    }

    @MethodAnnotation
    public void killAllHumans(){
        System.out.println("Bender is gonna kill all of the humans!");
    }

    @MethodAnnotation
    public void printMessage(@ParameterAnnotation String message){
        System.out.println(message);
    }

    @RepeatWithParameters(args = {"njin", "fnjoa", "fuds", "fasesaiud", "mmcieusi", "uiayg", "uqby"})
    public void print(String s){
        System.out.println(s);
    }

    public void printPurposeOfTheMethod(){
        System.out.println("Hello, I'm just a simple, not annotated method.");
    }
}
