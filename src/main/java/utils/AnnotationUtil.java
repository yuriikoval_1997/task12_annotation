package utils;

import custom_annotations.annotaion_for_task.ClassAnnotation;
import custom_annotations.annotaion_for_task.FieldAnnotation;
import custom_annotations.annotaion_for_task.MethodAnnotation;
import custom_annotations.annotaion_for_task.RepeatWithParameters;
import model.task_classes.AnnotatedClassSample;

import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;

public class AnnotationUtil {
    /**
     * This method takes any class and checks whether it is annotated with "ClassAnnotation".
     * If it is annotated, the method prints fields of the annotation. Otherwise prints that the class is not annotated.
     * @param clazz any class.
     */
    public static void inspectClass(Class<?> clazz) {
        if (clazz.isAnnotationPresent(ClassAnnotation.class)){
            ClassAnnotation classAnn = clazz.getAnnotation(ClassAnnotation.class);
            System.out.println(classAnn.getClass().getName() + ". Values: ");
            System.out.println(classAnn.idValue());
            System.out.println(classAnn.stringValue());
            System.out.println(classAnn.state());
        } else {
            System.out.println(clazz.getName() + " is not annotated with \"ClassAnnotation\"");
        }
    }


    /**
     * Checks whether the class is annotated with "ClassAnnotation".
     * If true, than the method creates an instance of the class and runs all of the class method annotated with
     * "MethodAnnotation" or "RepeatWithParameters".
     * @param clazz any class.
     * @throws IllegalAccessException
     * @throws InstantiationException
     * @throws InvocationTargetException
     */
    public static void runMarkedMethods(Class<?> clazz) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        AnnotatedClassSample acs;
        if (clazz.isAnnotationPresent(ClassAnnotation.class)){
            acs = (AnnotatedClassSample) clazz.newInstance();
            Method[] methods = clazz.getMethods(); // This gives only public methods of the class and its parent.
            // If you want all of the methods use getDeclaredMethod() and use recursion to go up hierarchily.
            for (Method m : methods){
                if (m.isAnnotationPresent(MethodAnnotation.class)){
                    if (m.getAnnotation(MethodAnnotation.class).invokeMethod()){
                        m.invoke(acs); // We need to pass an instance of the class in order to invoke non-static method.
                        // If you want to invoke a static method than pass null.
                    }
                } else if (m.isAnnotationPresent(RepeatWithParameters.class)){
                    String[] args = m.getAnnotation(RepeatWithParameters.class).args();
                    for (String s : args){
                        m.invoke(acs, s);
                    }
                }
            }
        }
    }

    public static void readAllFields(Class<?> clazz) throws IllegalAccessException, InstantiationException, InvocationTargetException {
        AnnotatedClassSample acs;
        if (clazz.isAnnotationPresent(ClassAnnotation.class)){
            acs = (AnnotatedClassSample) clazz.newInstance();
            Field[] fields = clazz.getDeclaredFields();
            for (Field f : fields){
                f.setAccessible(true);
                System.out.println(f.get(acs));
            }
        }
    }
}
