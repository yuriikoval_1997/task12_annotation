package custom_annotations.annotaion_for_task;

import java.lang.annotation.*;

@Documented
@Inherited
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface ClassAnnotation {
    int idValue() default -1;

    String stringValue();

    State state();
}
