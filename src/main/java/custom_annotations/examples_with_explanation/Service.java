package custom_annotations.examples_with_explanation;

import java.lang.annotation.*;

@Documented // It means that after generating javadoc for a class marked with this annotation (Service),
// there will be information that the class is marked with Service annotation
@Inherited // It means that all of the children of a class marked with this annotation (Service) will be marked by default
@Target(ElementType.TYPE)
@Retention(RetentionPolicy.RUNTIME)
public @interface Service {
    String name();

    boolean lazyLoad() default false;
}
