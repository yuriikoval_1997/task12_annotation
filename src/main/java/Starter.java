import model.task_classes.AnnotatedClassSample;
import model.task_classes.NotAnnotatedClass;
import utils.AnnotationUtil;

import java.lang.reflect.InvocationTargetException;

public class Starter {
    public static void main(String[] args) throws IllegalAccessException, InvocationTargetException, InstantiationException {
        AnnotationUtil.inspectClass(AnnotatedClassSample.class);
        AnnotationUtil.inspectClass(NotAnnotatedClass.class);
        AnnotationUtil.inspectClass(String.class);

        AnnotationUtil.runMarkedMethods(AnnotatedClassSample.class);
        AnnotationUtil.readAllFields(AnnotatedClassSample.class);
    }
}
